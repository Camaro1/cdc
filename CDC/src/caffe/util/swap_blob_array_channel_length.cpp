/*
 *
 *  When: 2016/09/03
 *  Who: Zheng Shou
 *  Where: Columbia University
 *
 */

#include <cmath>
#include <cstdlib>
#include <cstring>

#include "caffe/common.hpp"
#include "caffe/util/swap_blob_array_channel_length.hpp"

namespace caffe {

template <typename Dtype>
void swap_blob_array_channel_length_cpu(const Dtype* data_in, const int channels, const int length,
	    const int height, const int width, Dtype* data_out) {
			
  for (int c = 0; c < channels; ++c) {
    for (int l = 0; l < length; ++l) {
      for (int h = 0; h < height; ++h) {
        for (int w = 0; w < width; ++w) {
            data_out[ ((l * channels + c) * height + h) * width + w ] = data_in[ ((c * length + l) * height + h) * width + w ];
        }
      }
    }
  }
}

// Explicit instantiation
template void swap_blob_array_channel_length_cpu<float>(const float* data_in, const int channels, const int length,
    const int height, const int width, float* data_out);
template void swap_blob_array_channel_length_cpu<double>(const double* data_in, const int channels,const int length,
	    const int height, const int width, double* data_out);

}  // namespace caffe
