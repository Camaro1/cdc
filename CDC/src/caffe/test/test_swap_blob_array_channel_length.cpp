/*
 *
 *  When: 2016/09/03
 *  Who: Zheng Shou
 *  Where: Columbia University
 *
 */
 
#include <cstring>

#include "cuda_runtime.h"
#include "cublas_v2.h"

#include "gtest/gtest.h"
#include "caffe/blob.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/swap_blob_array_channel_length.hpp"

#include "caffe/test/test_caffe_main.hpp"

namespace caffe {

extern cudaDeviceProp CAFFE_TEST_CUDA_PROP;

typedef ::testing::Types<float, double> Dtypes;

template <typename Dtype>
class SwapTest : public ::testing::Test {};

TYPED_TEST_CASE(SwapTest, Dtypes);

TYPED_TEST(SwapTest, TestGemm) {
	
	// notes page 10
	// here we assume A(1,2,3,2,2)->swap->B(1,3,2,2,2)
  Blob<TypeParam> A(1,2,3,2,2);
  Blob<TypeParam> B(1,3,2,2,2);
  TypeParam data[24]  = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
  memcpy(A.mutable_cpu_data(), data, 24 * sizeof(TypeParam));
	
  if (sizeof(TypeParam) == 4 || CAFFE_TEST_CUDA_PROP.major >= 2) {
	  
	/*
	swap_blob_array_channel_length_cpu(A.cpu_data(),2,3,2,2,B.mutable_cpu_data());
    for (int i = 0; i < 24; ++i) {
		LOG(ERROR) << B.cpu_data()[i];
		}
	*/
	
	swap_blob_array_channel_length_gpu(A.gpu_data(),2,3,2,2,B.mutable_gpu_data());
    for (int i = 0; i < 24; ++i) {
		LOG(ERROR) << B.cpu_data()[i];
		}
	
  } else {
    LOG(ERROR) << "Skipping test due to old architecture.";	// LOG(ERROR) can output result but LOG(INFO) cannot in this file
  }
}


}  // namespace caffe
